'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

var path = {
    build: {
        html: 'build/',
        js: 'build/scripts/',
        css: 'build/styles/',
        img: 'build/images/',
        files: 'build/files/',
        fonts: 'build/fonts/',
        php: 'build/php/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/scripts/main.js',
        style: 'src/styles/main.scss',
        img: 'src/images/**/*.*',
        files: 'src/files/**/*.*',
        fonts: 'src/fonts/**/*.*',
        php: 'src/php/main.php'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/scripts/**/*.js',
        style: 'src/styles/**/*.scss',
        img: 'src/images/**/*.*',
        files: 'src/files/**/*.*',
        fonts: 'src/fonts/**/*.*',
        php: 'src/php/**/*.php'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: './build'
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: 'Bonauze'
};

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('scripts:build', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(uglify().on('error', function (error) {
            console.error('' + error);
        }))
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('scripts-development:build', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('styles:build', function () {
    gulp.src(path.src.style)
        .pipe(sass({
            includePaths: ['src/styles/'],
            outputStyle: 'compressed',
            sourceMap: true,
            errLogToConsole: true
        }).on('error', function (error) {
            console.error('' + error);
        }))
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('styles-development:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['src/styles/'],
            sourceMap: true,
            errLogToConsole: true
        }).on('error', function (error) {
            console.error('' + error);
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('images:build', function () {
    gulp.src(path.src.img)
        .pipe(imagemin({
			interlaced: true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('php:build', function () {
    gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php))
        .pipe(reload({stream: true}));
});

gulp.task('files:build', function() {
    gulp.src(path.src.files)
        .pipe(gulp.dest(path.build.files))
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
    'html:build',
    'scripts:build',
    'styles:build',
    'files:build',
    'fonts:build',
    'images:build',
    'php:build'
]);


gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('styles-development:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('scripts-development:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('images:build');
    });
    watch([path.watch.files], function(event, cb) {
        gulp.start('files:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
    watch([path.watch.php], function(event, cb) {
        gulp.start('php:build');
    });
});


gulp.task('default', ['build', 'webserver', 'watch']);