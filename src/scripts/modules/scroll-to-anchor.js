$('a[href^="#anchor-"]').on('click', function (e) {
    e.preventDefault();

    var $wrap     = $(this),
        id  	  = $wrap.attr('href'),
        top 	  = $(id).offset().top,
        animation = $wrap.data('animation-scroll'),
        speed     = 800;



    if (animation == false) {
        speed = 0;
    }

    $('html, body').animate({scrollTop : top}, speed);
});