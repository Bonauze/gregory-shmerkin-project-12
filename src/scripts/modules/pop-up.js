$('.js-pop-up').each(function () {
    var $wrap = $(this);



    $wrap.detach().appendTo('body');
});



$('a[href^=\"#pop-up\"]').on('click', function (e) {
    e.preventDefault();

    var namePopUp = $(this).attr('href');



    centerPopUp(namePopUp);

    $('.overlay, ' + namePopUp + '').fadeIn(150);
});



$('body').append('<div class="overlay">');

$(document).keydown(function (e) {
    if (e.keyCode === 27 ) {
        $('.overlay, .js-pop-up').fadeOut(150);
    }
});

$(document).delegate('.overlay, .js-pop-up-close', 'click', function (e) {
    e.preventDefault();

    $('.overlay, .js-pop-up').fadeOut(150);
});



function centerPopUp(namePopUp) {
    $(namePopUp).each(function() {
        var $wrap           = $(this),
            numberScrollTop = $(window).scrollTop(),
            heightWindow    = $(window).height(),
            heightPopUp     = $wrap.outerHeight(),
            total           = numberScrollTop + ((heightWindow-heightPopUp) / 2);



        if (heightPopUp > heightWindow) {
            $wrap.css({'top' : numberScrollTop+((heightWindow * 10 / 100))});
        } else {
            $wrap.css({'top' : total});
        }
    });
}